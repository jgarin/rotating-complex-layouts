package com.jgarin.rotationtestapp;


import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Use only support-v13 library as adapter parent class!
 * if we want Fragment states saved, use FragmentStatePagerAdapter!
 * other than that it's easy
 *
 * This is a simple ViewPagerAdapter
 * Created by haras_000 on 15-Aug-15.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> fragments;

    /**
     * @param fm - FragmentManager (no support fragment manager allowed!
     * @param fragments List<Fragment> - no support fragments allowed! passing null will create
     *                  an empty list
     */
    public ViewPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        if (fm == null) {
            throw new NullPointerException("FragmentManager cannot be null!");
        }
        if (fragments == null) {
            this.fragments = new ArrayList<Fragment>();
        } else {
            this.fragments = fragments;
        }
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
