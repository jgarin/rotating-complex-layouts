package com.jgarin.rotationtestapp;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * Use support library only for ViewPager view!!!
 * use getChildFragmentManager() method for all nested fragments!
 * it requires API 17 (4.2) or higher! otherwise viewpager won't work!
 * on screen rotation the adapter needs to be recreated, then its state is restored!
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment2 extends Fragment {

    private static final String TAG = "Fragment2";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private CallbackInterface callback;

    private ViewPagerAdapter adapter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment1.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment2 newInstance(String param1, String param2) {
        Log.d(TAG, "newInstance");
        Fragment2 fragment = new Fragment2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public Fragment2() {
        Log.d(TAG, "constructor");
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        if (savedInstanceState == null) {
            Log.wtf(TAG, "savedInstanceState null");
        }
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        if (savedInstanceState == null) {
            Log.wtf(TAG, "savedInstanceState null");
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_2, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated");
        ViewPager vp = (ViewPager) view.findViewById(R.id.vp);
        FragmentManager fm = getChildFragmentManager();
        List<Fragment> fragments = new ArrayList<Fragment>();
        fragments.add(Fragment3.newInstance("param1", "param2"));
        fragments.add(Fragment3.newInstance("param1", "param2"));
        fragments.add(Fragment3.newInstance("param1", "param2"));
        fragments.add(Fragment3.newInstance("param1", "param2"));
        fragments.add(Fragment3.newInstance("param1", "param2"));
        fragments.add(Fragment3.newInstance("param1", "param2"));
        adapter = new ViewPagerAdapter(fm, fragments);
        if (savedInstanceState != null) {
            adapter.restoreState(savedInstanceState.getParcelable("adapter"), adapter.getClass().getClassLoader());
        }
        vp.setAdapter(adapter);
//        btn = (Button) view.findViewById(R.id.btn);
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d(TAG, "onClick");
//                callback.executeAction(CallbackInterface.ACTION_ATTACH_FRAGMENT_3);
//            }
//        });
//        btn2 = (Button) view.findViewById(R.id.btn2);
//        btn2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentTransaction ft = getFragmentManager().beginTransaction();
//                Fragment fragment = Fragment4.newInstance("param1", "param2");
//                ft.replace(R.id.fragment_holder, fragment).commit();
//            }
//        });
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState");
        outState.putParcelable("adapter", adapter.saveState());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        Log.d(TAG, "onViewStateRestored");
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach");
        try {
            callback = (CallbackInterface) activity;
        } catch (ClassCastException e) {
            Toast.makeText(activity, activity.getLocalClassName() + " must implement CallbackInterface", Toast.LENGTH_SHORT).show();
        }
        super.onAttach(activity);
    }
}
