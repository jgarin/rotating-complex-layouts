package com.jgarin.rotationtestapp;

import android.app.Fragment;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

/**
 * We use getFragmentManager() method for Activities only!
 * no support libs for Fragments!
 */
public class MainActivity extends AppCompatActivity implements CallbackInterface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
//            Fragment fragment1 = Fragment1.newInstance("param1","param2");
//            FragmentTransaction ft1 = getFragmentManager().beginTransaction().replace(R.id.fragment1_holder, fragment1);
//            ft1.commit();
            Fragment fragment2 = Fragment2.newInstance("param1", "param2");
            FragmentTransaction ft2 = getFragmentManager().beginTransaction().replace(R.id.fragment2_holder, fragment2);
            ft2.commit();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void executeAction(int actionId) {
        Log.d("callback", "activity\'s executeAction method");
        switch (actionId) {
            case CallbackInterface.ACTION_ATTACH_FRAGMENT_3:
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment fragment = Fragment3.newInstance("param1", "param2");
                ft.replace(R.id.fragment1_holder, fragment).commit();
                break;
            case CallbackInterface.ACTION_TEST_CONNECTION:
                Toast.makeText(this, "Test successful!", Toast.LENGTH_SHORT).show();
                break;
            default: break;
        }
    }
}
