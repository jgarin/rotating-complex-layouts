package com.jgarin.rotationtestapp;

/**
 * Created by haras_000 on 15-Aug-15.
 */
public interface CallbackInterface {
    public static int ACTION_ATTACH_FRAGMENT_3 = 1;
    public static int ACTION_TEST_CONNECTION = 2;
    public static int ACTION_SWITCH_FRAGMENT_1 = 3;
    public static int ACTION_SWITCH_FRAGMENT_4 = 4;

    void executeAction(int actionId);
}
