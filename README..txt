A demo app to show proper screen rotation implementation
with nested fragments and view pager.

!Important
- use getFragmentManager() in Activity class
- use android.app.Fragment! 
- use support.v13 library for FragmentPagerAdapter and FragmentStatePagerAdapter
- the classes you want to save must implement Parcelable
